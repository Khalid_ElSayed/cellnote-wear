package de.frizzware.cellnote;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Frederik on 20.06.2014.
 */
public class CellNoteAdapter extends BaseAdapter{

    private LayoutInflater mInlInflater;
    private List items = new ArrayList();

    public CellNoteAdapter(Context context, List objects) {
        mInlInflater = LayoutInflater.from(context);
        Collections.sort(objects);
        insertSeperators(objects);
    }

    private void insertSeperators(List objects){
        ArrayList<CellNote> finished = new ArrayList<CellNote>();
        boolean under500 = true;

        if(objects.isEmpty()){
            items.add(new ListSeperator("Es ist noch ein wenig leer hier ;-)"));
            return;
        }

        for(int n = 0; n < objects.size(); n++){
            if(objects.get(n) instanceof CellNote){
                CellNote tmp = (CellNote) objects.get(n);

                if(tmp.isChecked){
                    finished.add(tmp);
                    continue;
                }

                if(n == 0 && tmp.distance < 500){
                    items.add(new ListSeperator("Ganz in Ihrer Nähe"));
                }else if(under500){
                    items.add(new ListSeperator("Ausstehende Notizen"));
                    under500 = false;
                }

                items.add(tmp);
            }
        }

        if(!finished.isEmpty()){
            items.add(new ListSeperator("Abgeschlossene Notizen"));
            for(CellNote c : finished){
                items.add(c);
            }
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Object current = getItem(position);

        if(current instanceof CellNote) {
            CellNote cn = (CellNote) current;
            convertView = mInlInflater.inflate(R.layout.listview_item, parent, false);

            ImageView bg = (ImageView) convertView.findViewById(R.id.imageViewItemBackground);
            FrameLayout frameDistance = (FrameLayout) convertView.findViewById(R.id.frameLayoutItemDistance);
            TextView textDistance = (TextView) convertView.findViewById(R.id.textViewItemDistance);
            TextView title = (TextView) convertView.findViewById(R.id.textViewItemTitle);
            TextView address = (TextView) convertView.findViewById(R.id.textViewItemAddress);

            bg.setImageBitmap(BitmapFactory.decodeFile(cn.mapfile));
            if(cn.isChecked){
                frameDistance.setBackgroundResource(R.drawable.bg_finished);
                textDistance.setVisibility(View.GONE);
            }else{
                frameDistance.setBackgroundResource(R.drawable.bg_ring);
                textDistance.setVisibility(View.VISIBLE);
            }
            textDistance.setText(LocationUtils.formatDist(cn.distance));
            title.setText(cn.title.toUpperCase());
            address.setText(cn.address.toUpperCase());
        }

        if(current instanceof ListSeperator){
            ListSeperator ls = (ListSeperator) current;
            convertView = mInlInflater.inflate(R.layout.listview_seperator, parent, false);

            convertView.setOnClickListener(null);
            convertView.setOnLongClickListener(null);
            convertView.setLongClickable(false);

            TextView title = (TextView) convertView.findViewById(R.id.textViewSeperator);
            title.setText(ls.title);
        }

        return convertView;
    }
}
