package de.frizzware.cellnote;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.preview.support.v4.app.NotificationManagerCompat;
import android.preview.support.wearable.notifications.RemoteInput;
import android.preview.support.wearable.notifications.WearableNotifications;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Frederik on 20.06.2014.
 */
public class NotificationUtils {

    public static final String EXTRA_NOTIFICATION_ID = "de.frizzware.cellnote.NOTIFICATION_ID",
                               EXTRA_CELLNOTE_ID = "de.frizzware.cellnote.CELLNOTE_ID",
                               EXTRA_MESSAGE = "de.frizzware.cellnote.MESSAGE";

    public static void showNotification(Context ctx, CellNote note){

        int currentNotificationID = (int) System.currentTimeMillis();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(ctx)
                        .setContentTitle(note.title)
                        .setContentText(note.text)
                        .setLargeIcon(BitmapFactory.decodeFile(note.mapfile))
                        .setSmallIcon(R.drawable.ic_launcher);

        RemoteInput remoteInput = new RemoteInput.Builder("voice").setLabel("Sprechen...").build();

        WearableNotifications.Action dismissAction =
                new WearableNotifications.Action.Builder(
                        R.drawable.ic_action_check,
                        "Abhaken",
                        getDismissPendingIntent(ctx, currentNotificationID, note.getId()))
                .build();

        WearableNotifications.Action remindAction =
                new WearableNotifications.Action.Builder(
                        R.drawable.ic_action_again,
                        "Erinnern",
                        getRemindPendingIntent(ctx, currentNotificationID))
                .build();

        WearableNotifications.Action addAction =
                new WearableNotifications.Action.Builder(
                        R.drawable.ic_action_add,
                        "Neue Notiz erstellen",
                        getAddNewPendingIntent(ctx, currentNotificationID))
// TODO This is just a workaround for the emulator - uncomment when running on a real device
//                .addRemoteInput(remoteInput)
                .build();

        Notification notification = new WearableNotifications.Builder(notificationBuilder)
                .addAction(dismissAction)
                .addAction(remindAction)
                .addAction(addAction)
                .build();

        NotificationManagerCompat manager = NotificationManagerCompat.from(ctx);
        manager.notify(currentNotificationID, notification);
    }

    public static PendingIntent getShowNotificationPendingIntent(Context ctx, long cellNoteID){
        Intent dismissIntent = new Intent(ctx, NotificationIntentReceiver.class);
        dismissIntent.setAction(NotificationIntentReceiver.ACTION_SHOW_NOTIFICATION);
        dismissIntent.putExtra(EXTRA_CELLNOTE_ID, cellNoteID);
        return PendingIntent.getBroadcast(ctx, (int) cellNoteID, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent getDismissPendingIntent(Context ctx, int notificationID, long cellNoteID){
        Intent dismissIntent = new Intent(ctx, NotificationIntentReceiver.class);
        dismissIntent.setAction(NotificationIntentReceiver.ACTION_DISMISS_CELLNOTE);
        dismissIntent.putExtra(EXTRA_NOTIFICATION_ID, notificationID);
        dismissIntent.putExtra(EXTRA_CELLNOTE_ID, cellNoteID);
        return PendingIntent.getBroadcast(ctx, notificationID, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent getRemindPendingIntent(Context ctx, int notificationID){
        Intent dismissIntent = new Intent(ctx, NotificationIntentReceiver.class);
        dismissIntent.setAction(NotificationIntentReceiver.ACTION_REPEAT_CELLNOTE);
        dismissIntent.putExtra(EXTRA_NOTIFICATION_ID, notificationID);
        return PendingIntent.getBroadcast(ctx, notificationID, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent getAddNewPendingIntent(Context ctx, int notificationID){
        Intent dismissIntent = new Intent(ctx, NotificationIntentReceiver.class);
        dismissIntent.setAction(NotificationIntentReceiver.ACTION_NEW_CELLNOTE);
        return PendingIntent.getBroadcast(ctx, notificationID, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
