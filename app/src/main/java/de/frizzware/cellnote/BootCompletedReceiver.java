package de.frizzware.cellnote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by Frederik Schweiger on 25.06.2014.
 */
public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ArrayList<CellNote> notes = CellNote.all(CellNote.class);

        for(CellNote tmp : notes){
            tmp.register(context);
        }
    }

}
